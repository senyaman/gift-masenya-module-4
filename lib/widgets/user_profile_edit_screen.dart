import 'package:flutter/material.dart';
import 'package:flutter_app/widgets/second_feature_screen.dart';

class UserProfile extends StatelessWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Profile'),
      ),
      body: Column(
        children: [
          Container(
            width: 350,
            height: 45,
            margin: const EdgeInsets.only(left: 20, top: 15, right: 20),
            child: const TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                label: Text('Change username'),
              ),
            ),
          ),
          Container(
            width: 350,
            height: 45,
            margin: const EdgeInsets.only(left: 20, top: 15, right: 20),
            child: const TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                label: Text('Change password'),
              ),
            ),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const SecondFeature()));
            },
            child: const Text('Feature screen'),
          ),
        ],
      ),
    );
  }
}
