import 'package:flutter/material.dart';
import 'package:flutter_app/widgets/login_screen.dart';

class Registration extends StatelessWidget {
  const Registration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Registration'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 350,
            height: 45,
            margin: const EdgeInsets.only(left: 20, top: 15, right: 20),
            child: const TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                label: Text('First name'),
              ),
            ),
          ),
          Container(
            width: 350,
            height: 45,
            margin: const EdgeInsets.only(left: 20, top: 18, right: 20),
            child: const TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                label: Text('Last name'),
              ),
            ),
          ),
          Container(
            width: 350,
            height: 45,
            margin: const EdgeInsets.only(left: 20, top: 18, right: 20),
            child: const TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                label: Text('Email'),
              ),
            ),
          ),
          Container(
            width: 350,
            height: 45,
            margin: const EdgeInsets.only(left: 20, top: 18, right: 20),
            child: const TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                label: Text('Password'),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 20, top: 0, right: 20),
            child: const Text(
              'Minimum length is 8 characters',
            ),
          ),
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.only(top: 10),
            child: ElevatedButton(
              child: const Text('Register'),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const LoginScreen()));
              },
            ),
          ),
        ],
      ),
    );
  }
}
