import 'package:flutter/material.dart';
import 'package:flutter_app/widgets/user_profile_edit_screen.dart';

class AnotherScreen extends StatelessWidget {
  const AnotherScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Feature screen'),
      ),
      body: Column(
        children: [
          const Text('Needs some implementation'),
          ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const UserProfile()));
            },
            child: const Text('Edit profile'),
          ),
        ],
      ),
    );
  }
}
