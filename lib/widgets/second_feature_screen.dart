import 'package:flutter/material.dart';

class SecondFeature extends StatelessWidget {
  const SecondFeature({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Second feature'),
      ),
    );
  }
}
